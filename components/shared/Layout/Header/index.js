import MenuList from '../Menu/MenuList';
import Link from 'next/link';
import { useState } from 'react';
const Header = () => {
  const [show, setShow] = useState(false);
  const [searchBtn, setSearchBtn] = useState(false);
  return (
    <section id='header'>
      <div
        className={`mobile-menu ${
          show ? 'mobile-menu-show' : 'mobile-menu-close'
        }`}
      >
        <div className='mobile-list-box'></div>
      </div>
      <div className='row header-box'>
        <div className='col-xl-9  header-box-col'>
          <Link href='/'>
            <a className='header-logo-box'>
              <img src='/static/img/home/main-logo.png' alt='' />
            </a>
          </Link>
          <div className='infobar d-none d-lg-block'>
            <div className='infobar-list'>
              <ul>
                <li>
                  <Link href='/'>
                    <a>網站導覽</a>
                  </Link>
                </li>
              </ul>
              <ul>
                <li>
                  <Link href='/'>
                    <a>字級</a>
                  </Link>
                </li>
              </ul>
              <ul>
                <li
                  className='search-btn'
                  onClick={() => setSearchBtn(!searchBtn)}
                >
                  <Link href='/'>
                    <a>
                      <img src='/static/img/icons/search_icon.png' alt='' />
                      搜尋
                    </a>
                  </Link>
                  <div className={`search-bar ${searchBtn ? 'show' : 'close'}`}>
                    <label></label>
                    <input
                      type='text'
                      name=''
                      id=''
                      title='請輸入關鍵字'
                      placeholder='請輸入關鍵字'
                    />
                    <button type='button' className='btn btn-outline-light'>
                      搜尋
                    </button>
                  </div>
                </li>
              </ul>
              <ul>
                <li>
                  <Link href='/'>
                    <a>分享</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className='d-block d-lg-none' onClick={() => setShow(!show)}>
            <div
              className={`mobile-btn  ${
                show ? 'mobile-btn-show' : 'mobile-btn-close'
              }`}
            >
              <div className='burger'></div>
            </div>
            {/* <img src='/static/img/icons/mobile-btn.png' alt='' /> */}
          </div>
        </div>
      </div>
      <MenuList />
    </section>
  );
};
export default Header;
