import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

const StockTable = ({ res }) => {
  const classes = useStyles();
  let data = res ? res : false;
  return (
    <Grid container justify="center">
      <Grid md={10}>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>證券代號</TableCell>
                <TableCell align="right">證券名稱</TableCell>
                <TableCell align="right">開盤價</TableCell>
                <TableCell align="right">最高價</TableCell>
                <TableCell align="right">最低價</TableCell>
                <TableCell align="right">收盤價</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data?.map((row, index) => (
                <TableRow key={index}>
                  <TableCell component="th" scope="row">
                    {row.證券代號}
                  </TableCell>
                  <TableCell align="right">{row.證券名稱}</TableCell>
                  <TableCell align="right">{row.開盤價}</TableCell>
                  <TableCell align="right">{row.最高價}</TableCell>
                  <TableCell align="right">{row.最低價}</TableCell>
                  <TableCell align="right">{row.收盤價}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
};
export default StockTable;
