import App from 'next/app';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-datepicker/dist/react-datepicker.min.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import '../public/static/css/main.css';
import UserContext from '../components/shared/Layout/MainContent/UserContext';
export default class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <UserContext.Provider theme={{ theme: '#999', data: 'test' }}>
        <Component {...pageProps} />
      </UserContext.Provider>
    );
  }
}
