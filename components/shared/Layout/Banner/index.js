import Slide from 'react-slick';
const Page = () => {
  const setting = {
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: false,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  let imgData = [];
  imgData = [
    '/static/img/banner/1.jpg',

    '/static/img/banner/2.jpg',
    '/static/img/banner/3.jpg',
    '/static/img/banner/4.jpg',
    '/static/img/banner/5.jpg',
    '/static/img/banner/6.jpg',
    '/static/img/banner/7.jpg',
    '/static/img/banner/8.jpg',
  ];
  // console.log('imgDATA:', imgData);
  return (
    <section id='banner' className=''>
      {/* <h2>Slick</h2> */}
      <Slide {...setting}>
        {imgData.map((item, index) => (
          <div key={index} className=''>
            <div
              className='banner-image'
              style={{
                background: `url(${item}) no-repeat center center`,
                backgroundSize: 'cover',
              }}
            ></div>
          </div>
        ))}
      </Slide>
    </section>
  );
};
export default Page;
