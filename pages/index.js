import { fetchData } from '../api/helper';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../components/shared/Layout/MainContent/UserContext';
import Layout from '../components/shared/Layout';
import Link from 'next/link';
import Banner from '../components/shared/Layout/Banner';

const Index = (props) => {
  // const { theme, data } = useContext(UserContext);
  // console.log('theme',theme)
  // const [settingTime, setSettingTime] = useState(5000);
  // let obj = {
  //   time: new Date().getTime(),
  //   name: 'arvin',
  //   age: '40',

  // };
  // let str = JSON.stringify(obj);

  // console.log('props:', props);
  return (
    <Layout>
      <Banner />
      <div className="container ">
        <div className="row">
          <div className="col-12 mt-5">
            <Link href="/datepicker">
              <a className="mx-3">
                <button className="btn btn-outline-dark">DatePicker</button>
              </a>
            </Link>
            <Link href="/slick">
              <a className="mx-3">
                <button className="btn btn-outline-dark">react-slick</button>
              </a>
            </Link>
            <Link href="/register">
              <a className="mx-3">
                <button className="btn btn-outline-dark">會員註冊</button>
              </a>
            </Link>
            <Link href="/apiTest">
              <a className="mx-3">
                <button className="btn btn-outline-dark">個股日成交資料</button>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
};
// export async function getStaticProps() {
//   const bannerData = await fetchData('/api/banner');
//   return {
//     props: {
//       bannerData,
//     },
//   };
// }
export default Index;
