const MenuList = () => {
  return (
    <div className='row menu-box '>
      <div className='col-lg-9 d-none d-lg-block '>
        <div className='menu-list'>
          <ul>
            <li>認識北埔</li>
          </ul>
          <ul>
            <li>公所簡介</li>
          </ul>
          <ul>
            <li>公佈欄</li>
          </ul>
          <ul>
            <li>政府資訊公開</li>
          </ul>
          <ul>
            <li>便民服務</li>
          </ul>
          <ul>
            <li>建設農業與社區</li>
          </ul>
          <ul>
            <li>北埔鄉民代表會</li>
          </ul>
        </div>
      </div>
    </div>
  );
};
export default MenuList;
