export const links = [
  {
    title: '認識北埔',
    path: '/',
    subNav: [
      {
        tiele: '歷史沿革',
        path: '/',
      },
    ],
  },
  {
    title: '公所簡介',
    path: '/',
    subNav: [
      {
        tiele: '歷史沿革',
        path: '/',
      },
    ],
  },
  {
    title: '公佈欄',
    path: '/',
    subNav: [
      {
        tiele: '歷史沿革',
        path: '/',
      },
    ],
  },
  {
    title: '政府資訊公開',
    path: '/',
    subNav: [
      {
        tiele: '歷史沿革',
        path: '/',
      },
    ],
  },
  {
    title: '便民服務',
    path: '/',
    subNav: [
      {
        tiele: '歷史沿革',
        path: '/',
      },
    ],
  },
  {
    title: '建設農業與社區',
    path: '/',
    subNav: [
      {
        tiele: '歷史沿革',
        path: '/',
      },
    ],
  },
  {
    title: '北埔鄉民代表會',
    path: '/',
    subNav: [
      {
        tiele: '歷史沿革',
        path: '/',
      },
    ],
  },
];
