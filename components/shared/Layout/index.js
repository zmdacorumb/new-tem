import Head from 'next/head';
import MainContent from './MainContent';
import Header from './Header';
import { getStaticProps } from '../../../pages';
import Footer from './Footer/index';
const Layout = (props) => {
  return (
    <React.Fragment>
      <Head>
        <title>北埔鄉公所</title>
        <link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
      </Head>
      <div id='app' className='container-fluid app'>
        <a
          href='#C'
          id='gotocenter'
          title='跳到主要內容'
          tabIndex='1'
          className='sr-only sr-only-focusable'
        >
          跳到主要內容
        </a>
        <noscript>
          您的瀏覽器已關閉Javascript語法，開啟後即可正常瀏覽！ JavaScript has
          been deactivated on your browser, once turned on, you can browse
          normally!
        </noscript>
        <div className='page-wrap row'>
          <Header />
          <MainContent content={props.children} />
          <Footer />
        </div>
      </div>
    </React.Fragment>
  );
};
export default Layout;
