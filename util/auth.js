/**
 * All User & Authentication Related Functions
 */

import { postData, postAuth, fetchAuth } from '../api/helper';
import Router from 'next/router';

/**
 * Logs in user.
 * @param account - account name entered by user in input
 * @param password - password entered by user in input
 */
export const loginUser = async (account, password) => {
  // attempts login
  const response = await postData('/api/MemberAccount/Login', {
    Account: account,
    Password: password,
    // CaptchaID: captcha,
    // CaptchaCode: verification,
  });

  // returns a regular promise or rejected promise if there's an error with the login
  return response;
};

/**
 * Logs out user.
 * @param redirect - dictates whether or not the user should be redirected to login page after being logged out
 */
export const logoutUser = async (redirect) => {
  console.log('Attempting to Logout User');

  // logs out user from server side
  // const response = await fetchAuth("/api/Logout");

  if (typeof window !== 'undefined') {
    // removing both token and user objects in local storage before logout
    localStorage.removeItem(process.env.token);
    // localStorage.removeItem(process.env.user);
  }

  if (redirect) {
    // redirect to login page
    Router.push('/member-area/login');
  }
};

/**
 * Stores user token in localStorage.
 * @param response - response after logging in
 */
export const storeUser = async (res) => {
  const { User_Access_Token: Token = {} } = res.data;

  if (Token && typeof window !== 'undefined') {
    localStorage.setItem(process.env.token, Token);
    // localStorage.setItem(process.env.user, JSON.stringify(Data));

    // set user data
    const memberData = await postAuth('/api/MemberAccount/GetMemberDetail', {
      AccessToken: Token,
    });
    localStorage.setItem(process.env.user, JSON.stringify(memberData));

    // sucessful therefore return to stop rest of function
    console.log('Login Successful. Stored User.');
    return true;
  }
  console.log(
    'There was a problem with the login. User login response does not contain token.'
  );
  // no token and / or data therefore return error
  return false;
};

/**
 * Acquires user token from localStorage.
 */
export const getToken = () => {
  if (typeof window !== 'undefined') {
    const isAuthenticated = userAuthenticated().authenticated;
    // console.log('Is Authenticated:', isAuthenticated);

    // check for authenticated user and on client side before return token

    console.log('isAuthenticated:', isAuthenticated);
    if (isAuthenticated) {
      return localStorage.getItem(process.env.token);
    }
  }
};

/**
 * Acquires user info from localStorage.
 */
export const getUser = () => {
  if (typeof window !== 'undefined') {
    const isAuthenticated = userAuthenticated().authenticated;
    // console.log('Is Authenticated:', isAuthenticated);

    // check for authenticated user and on client side before return token
    if (isAuthenticated) {
      return JSON.parse(localStorage.getItem(process.env.user));
    }
  }

  // else return null
  return null;
};

/**
 * Checks if user is authenticated. Returns an object with admin role and
 * authenticated status (BOOLEAN).
 */
export const userAuthenticated = () => {
  if (typeof window !== 'undefined') {
    const token = localStorage.getItem(process.env.token);
    // const user = JSON.parse(localStorage.getItem(process.env.user));

    const isAuthenticated = token !== null;
    // console.log('Is Authenticated:', isAuthenticated);

    const authenticatedObj = {
      authenticated: false,
    };

    // console.log("isAuthenticated", isAuthenticated);

    if (isAuthenticated) {
      // Comparing Local Config Role and Stored User Role
      // console.log(
      //   'COMPARING localStorage RoleID 1 and process.env.roles.admin:',
      //   user.RoleID[0],
      //   process.env.roles.admin
      // );
      authenticatedObj.authenticated = true;

      /**
       * Returned object indicates logged-in status and user object
       * Example:
       * {
       *  admin: true,
       *  authenticated: true
       * }
       */
      return authenticatedObj;
    } else {
      // return false to indicate user not authenticated
      return authenticatedObj;
    }
  }
};
