import { fetchData } from '../api/helper';
import StockTable from '../components/Table/StockTable';
import { useState, useEffect } from 'react';
import Layout from '../components/shared/Layout/index';

const ApiTest = () => {
  const [res, setRes] = useState('');

  useEffect(() => {
    let data;
    const getData = async () => {
      data = await fetchData(
        // 'https://quality.data.gov.tw/dq_download_json.php?nid=11549&md5_url=bb878d47ffbe7b83bfc1b41d0b24946e'
        // 'https://data.moi.gov.tw/MoiOD/System/DownloadFile.aspx?DATA=B7BBE544-EBFB-41F7-93FD-6996FD24767E'
        'https://www.ydn.com.tw/api/video'
      );
      if (data) {
        setRes(data);
      }
    };
    getData();
  }, []);
  console.log('res', res ? res : '無資料');
  return <Layout>{/* <StockTable res={res} /> */}</Layout>;
};
export default ApiTest;
