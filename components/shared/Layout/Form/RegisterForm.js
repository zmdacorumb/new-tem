import Router from 'next/router';
import { useState, useEffect } from 'react';
import {
  Button,
  Paper,
  Grid,
  Box,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from '@material-ui/core';

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import { withStyles } from '@material-ui/core/styles';
// import Button from "@material-ui/core/Button";

import { makeStyles } from '@material-ui/core/styles';

import { postData } from '../../../../api/helper';
import { loginUser, storeUser } from '../../../../util/auth';
import { withFormik } from 'formik';
import Link from 'next/link';
// import ValidateCode from '../../shared/Layout/MainContent/ValidateCode';

import moment from 'moment';
const formStyle = makeStyles((theme) => ({
  title: {
    display: 'block',
    justifyContent: 'space-between',
    padding: '10px 0',
    alignItems: 'center',
    '& h1': {
      fontSize: '2.4rem',
      borderBottom: '1px solid #000',
      paddingBottom: '12px',
      textAlign: 'center',
      [theme.breakpoints.up('md')]: {
        borderBottom: 'none',
        textAlign: 'left',
        fontSize: '3.4rem',
        paddingBottom: '0',
      },
    },
    [theme.breakpoints.up('md')]: {
      borderBottom: '1px solid #000',
      display: 'flex',
    },
  },
  contentOuter: {
    width: '100%',
    margin: 'auto',
    [theme.breakpoints.up('md')]: {
      width: '85%',
    },
  },
  section: {
    padding: '20px 0 0 0',
    [theme.breakpoints.up('sm')]: {
      padding: '30px 0 0 0',
    },
  },
  form: {
    width: '100%',
    margin: 'auto',
    paddingTop: '15px',
    paddingBottom: '25px',
    [theme.breakpoints.up('sm')]: {
      minWidth: '388px',
      width: '50%',
    },
  },
  checkobox: {
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    "& input[type='checkbox']": {
      margin: '0 5px',
      width: '20px',
      height: '20px',
    },
    '& label': {
      marginBottom: '0 !important',
      '&[required]::before': {
        content: '"*"',
        display: 'block',
        color: 'red',
      },
    },
  },
  formGroup: {
    display: 'block',
    margin: '20px 0',
    [theme.breakpoints.up('sm')]: {
      margin: '40px 0',
      display: 'flex',
    },
    '& input,& textarea,& select': {
      // flexGrow: '1',
      width: '100%',
      padding: '4px 8px',
      borderRadius: '2px',
    },
    '& input:not([id=birth]),& textarea,& select': {
      border: '1px solid #979797',
    },
    '& input[id=birth]': {
      // flexGrow: '1',
      width: '100%',
      padding: '4px 8px',
      borderRadius: '2px',
    },
    '& label': {
      minWidth: '135px',
      margin: '0 5px 0 0',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
      [theme.breakpoints.up('sm')]: {
        justifyContent: 'flex-end',
      },
    },
    "& input[type='radio']": {
      width: '5%',
      margin: '0 3px',
      minWidth: '30px',
    },
  },
  datePick: {
    width: '100%',
    '& input': {
      padding: '6px 7px 0 7px',
      fontSize: '2rem',
    },
  },
  formValidation: {
    color: 'red',
    margin: '10px 0',
    fontSize: '1.4rem',
    textAlign: 'right',
  },
  buttonWrap: {
    display: 'flex',
    justifyContent: 'center',
  },
  submitButton: {
    background: '#201e20',
    fontSize: '1.8rem',
    color: '#f4a000',
    borderRadius: '2px',
    padding: '10px 0',
    width: '80%',
    marginTop: '40px',
    [theme.breakpoints.up('sm')]: {
      width: '40%',
    },
  },
  registerButton: {
    background: '#f4a000',
    fontSize: '1.8rem',
    color: '#201e20',
    borderRadius: '2px',
    padding: '10px 0',
    width: '100%',
    marginTop: '20px',
    [theme.breakpoints.up('sm')]: {
      width: '76.5%',
    },
  },
  nonMemberMsg: {
    marginTop: '20px',
    fontSize: '1.8rem',
  },
  radio: {
    alignItems: 'center',
  },
  radioBox: {
    width: '100%',
    display: 'block',
    display: 'flex',
    flexDirection: 'culenm',
    // justifyContent: "center",
    alignItems: 'center',
    '& label': {
      display: 'inline-block',
    },
  },
  radioLabel: {
    minWidth: 'auto !important',
  },
}));

const RegisterForm = (props) => {
  const classes = formStyle();

  const {
    popup,
    // formik
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setFieldValue,
    setValues,
    handleReset,
    dirty,
    handleClose,
    isValidating,
  } = props;

  const [showPassword, setShowPassword] = useState(false);

  useEffect(() => {
    const focusBtn = document.querySelector('.close-btn');
    if (focusBtn) {
      focusBtn.focus();
    }
  }, []);

  let handleBirth = (e) => {
    let year, month, day;
    let date = moment(e).format('YYYY-MM-DD');
    if (date) {
      year = date.substr(0, 4);
      month = date.substr(5, 2);
      day = date.substr(8, 2);
    }

    setValues({
      ...values,
      ['birth_year']: year,
      ['birth_month']: month,
      ['birth_day']: day,
      ['birth']: date,
    });
  };

  // 表單input有問題時跳到第一個input
  useEffect(() => {
    const keys = Object.keys(errors);
    console.log('keys', keys);
    if (keys.length > 0 && isSubmitting && !isValidating) {
      const errorElement = document.querySelector(`[id="${keys[0]}"]`);
      errorElement.focus();
    }
  }, [Object.keys(errors)]);

  // 直接用formik的handleChange會出錯 只好間接塞入
  const dateChange = (e) => {
    setFieldValue('birth', e);
  };
  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <div className={classes.formGroup}>
        <label htmlFor='name'>會員中文姓名 : </label>
        <input
          type='text'
          id='name'
          name='name'
          value={values.name}
          onChange={handleChange}
        />
      </div>
      {/* validation 驗證 */}
      {errors.name && touched.name && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.name}
        </div>
      )}

      <div className={classes.formGroup}>
        <label htmlFor='idNumber'>身份證字號 : </label>
        <input
          type='text'
          id='idNumber'
          name='idNumber'
          value={values.idNumber}
          onChange={handleChange}
        />
      </div>
      {/* validation 驗證 */}
      {errors.idNumber && touched.idNumber && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.idNumber}
        </div>
      )}

      <div className={classes.formGroup}>
        <label htmlFor='mail'>Email : </label>
        <input
          type='email'
          id='mail'
          name='mail'
          value={values.mail}
          onChange={handleChange}
        />
      </div>
      {/* validation 驗證 */}
      {errors.mail && touched.mail && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.mail}
        </div>
      )}

      <div className={classes.formGroup}>
        <label htmlFor='phone'>電話 : </label>
        <input
          type='phone'
          id='phone'
          name='phone'
          value={values.phone}
          onChange={handleChange}
        />
      </div>
      {/* validation 驗證 */}
      {errors.phone && touched.phone && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.phone}
        </div>
      )}

      <div className={classes.formGroup}>
        <label htmlFor='mobilePhone'>手機 : </label>
        <input
          type='phone'
          id='mobilePhone'
          name='mobilePhone'
          value={values.mobilePhone}
          onChange={handleChange}
        />
      </div>
      {/* validation 驗證 */}
      {errors.mobilePhone && touched.mobilePhone && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.mobilePhone}
        </div>
      )}

      <div className={classes.formGroup}>
        <label htmlFor='birth'>出生日期 : </label>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            className={classes.datePick}
            // margin="normal"
            id='birth'
            name='birth'
            label='出生日期'
            format='MM/dd/yyyy'
            value={values.birth}
            onChange={handleBirth}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
          />
        </MuiPickersUtilsProvider>
        {/* <input
          type="date"
          id="birth"
          name="birth"
          value={values.birth}
          onChange={(e) => handleBirth(e)}
        /> */}
      </div>
      {/* validation 驗證 */}
      {errors.birth && touched.birth && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.birth}
        </div>
      )}
      <div className={`${classes.formGroup} ${classes.radio}`}>
        <label>性別 : </label>
        <Grid container className={classes.radioBox}>
          <Grid item>
            <label className={classes.radioLabel} htmlFor='male'>
              <input
                id='male'
                type='radio'
                onChange={handleChange}
                value='男'
                name='gender'
                defaultChecked={true}
              />
              男
            </label>
          </Grid>

          <Grid item>
            <label className={classes.radioLabel} htmlFor='female'>
              <input
                id='female'
                type='radio'
                onChange={handleChange}
                value='女'
                name='gender'
              />
              女
            </label>
          </Grid>
        </Grid>
      </div>
      {/* validation 驗證 */}
      {errors.gender && touched.gender && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.gender}
        </div>
      )}
      <div className={`${classes.formGroup} ${classes.radio}`}>
        <label>是否接收線上DM : </label>
        <Grid container className={classes.radioBox}>
          <Grid item>
            <label className={classes.radioLabel} htmlFor='yesdm'>
              <input
                id='yesdm'
                type='radio'
                onChange={handleChange}
                value='true'
                name='dm'
                defaultChecked={true}
              />
              是
            </label>
          </Grid>

          <Grid item>
            <label className={classes.radioLabel} htmlFor='nodm'>
              <input
                id='nodm'
                type='radio'
                onChange={handleChange}
                value='false'
                name='dm'
              />
              否
            </label>
          </Grid>
        </Grid>
      </div>
      {/* validation 驗證 */}
      {errors.dm && touched.dm && (
        <div className={`${classes.formValidation} fade-left`}>{errors.dm}</div>
      )}

      <div className={classes.formGroup}>
        <label htmlFor='password'>密碼 : </label>
        <input
          type='password'
          id='password'
          name='password'
          value={values.password}
          onChange={handleChange}
        />
      </div>
      {/* validation 驗證 */}
      {errors.password && touched.password && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.password}
        </div>
      )}

      <div className={classes.formGroup}>
        <label htmlFor='ckpassword'>再次確認密碼 : </label>
        <input
          type='password'
          id='ckpassword'
          name='ckpassword'
          value={values.ckpassword}
          onChange={handleChange}
        />
      </div>
      {/* validation 驗證 */}
      {errors.ckpassword && touched.ckpassword && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.ckpassword}
        </div>
      )}
      <div className={classes.checkobox}>
        <input
          type='checkbox'
          id='terms'
          name='terms'
          value={values.terms}
          onChange={handleChange}
        />
        <label htmlFor='terms'>接受會員條款及個人註冊保護條款</label>
      </div>
      {/* validation 驗證 */}
      {errors.terms && touched.terms && (
        <div className={`${classes.formValidation} fade-left`}>
          {errors.terms}
        </div>
      )}

      <div className={classes.buttonWrap}>
        <Button
          type='submit'
          data-action='confirm'
          color='primary'
          className={classes.submitButton}
        >
          註冊
        </Button>
      </div>
    </form>
  );
};

const RegisterFormValidated = withFormik({
  // allows Formik to reinitialize it's forms when props change
  enableReinitialize: true,

  // Makes values props that holds the form state
  mapPropsToValues: (props) => {
    return {
      name: '',
      idNumber: '',
      mail: '',
      phone: '',
      mobilePhone: '',
      gender: '男',
      dm: 'true',
      password: '',
      ckpassword: '',
      birth: null,
      birth_year: '',
      birth_month: '',
      birth_day: '',
      terms: '', //接收條款
      // captchaID: '', // !!!
      // captchaCode: '',
    };
  },
  // Custom validation
  validate: (values) => {
    console.log('values:', values);
    const errors = {};

    if (!values.name) {
      errors.name = '必填欄位';
    } else if (values.name.match(/ /g)) {
      errors.name = '姓名欄位不能有空白';
    }
    if (!values.idNumber) {
      errors.idNumber = '必填欄位';
    }
    if (!values.mail) {
      errors.mail = '必填欄位';
    }
    if (!values.phone) {
      errors.phone = '必填欄位';
    }
    if (!values.mobilePhone) {
      errors.mobilePhone = '必填欄位';
    }
    if (!values.birth) {
      errors.birth = '必填欄位';
    }

    if (!values.gender) {
      errors.gender = '必填欄位';
    }
    if (!values.dm) {
      errors.dm = '必填欄位';
    }

    if (!values.password) {
      errors.password = '必填欄位';
    } else if (values.password.length < 8 || values.password.length > 12) {
      errors.password = '密碼必須介於8至12位數，且含有大寫和小寫英文字母';
    } else if (!values.password.match(/^(?=.*?[a-z]).*$/)) {
      errors.password = '密碼必須最少包含一個"小寫"英文字母';
    } else if (!values.password.match(/^(?=.*?[A-Z]).*$/)) {
      errors.password = '密碼必須最少包含一個"大寫"英文字母';
    } else if (!values.password.match(/^(?=.*?[0-9]).*$/)) {
      errors.password = '密碼必須最少包含一個"數字';
    } else if (values.password.match(/^(?=.*?[!@#$%)(^&.]).*$/)) {
      errors.password = '密碼不能包含"!@#$%^&."特殊符號';
    }
    if (!values.ckpassword) {
      errors.ckpassword = '必填欄位';
    } else if (values.ckpassword !== values.password) {
      errors.ckpassword = '請確定是否輸入正確';
    }
    if (!values.terms) {
      errors.terms = '必填欄位';
    }

    return errors;
  },

  // Submitting Form
  handleSubmit: async (values, { props, setSubmitting }) => {
    console.log('Logging in user.');
    const postObj = {
      name: values.name,
      identifier: values.idNumber,
      email: values.mail,
      contact_tel: values.phone,
      mobile: values.mobilePhone,
      date: values.birth,
      gender: values.gender,
      accept_benefit: values.dm,
      Password: values.password,
      terms: values.terms, //接收條款欄位還要確認該傳至哪個欄位

      // CaptchaID: values.CaptchaID,
      // CaptchaCode: values.captchaCode,
    };
    console.log('postObj', postObj);
    console.log('props', props);
    props.handleToggleLoading();
    let res = await postData('/api/MemberAccount/RegisterMember', postObj);
    props.handleToggleLoading();
    console.log('postObjres', res);
    console.log('res.status', typeof res);

    if (res.status === 200) {
      // const { dialogModal } = props;
      setSubmitting(false);
      console.log('===200');
      props.dialogModal({
        type: 'alert',
        title: '',
        content: '註冊成功',
        handleAfterAction: (value) => {
          // value is confirm or cancel
          Router.push('/');
        },
      });
      setSubmitting(false);
    } else {
      if (res === 406) {
        props.dialogModal('', '驗證碼錯誤');
      } else {
        props.dialogModal('', '註冊失敗,請諮詢相關人員');
      }
    }
    // loginUser(postObj)
    //   .then((res) => {
    // successful response object stored in local storage
    // const userStored = storeUser(res);

    // if (userStored) {
    // redirect to home after account logged in
    // props.setIsLoading(false);
    //   props.setSuccessModal({
    //     msg: "註冊成功!",
    //     url: "/",
    //   });
    // } else {
    // props.setIsLoading(false);

    //   console.log("Throwing Error");
    //   return Promise.reject();
    // }
    // setSubmitting(false);

    // end loading animation
    // })
    // .catch((err) => {
    // props.setIsLoading(false);
    // props.setSuccessModal({
    //   msg: "登入沒成功, 請檢查登入資料或網路",
    //   error: true,
    // });
    console.log('Error response from server:', err);
    // });
  },

  displayName: 'BasicForm',
})(RegisterForm);

export default RegisterFormValidated;
