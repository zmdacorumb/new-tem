import api from './api';

export const fetchData = async (endpoint) => {
  let response = [];

  try {
    response = await api.get(endpoint);
  } catch (err) {
    console.log('Error caught with GET request', err);
    return err;
  }
  console.log('Response Data:', response);
  return response.data;
};

export const postData = async (endpoint, payload) => {
  let response = [];

  // if backend needs querystring
  const postObj = qs.stringify(payload);
  // UNCOMMENT if backend needs JSON, COMMENT above
  // const postObj = JSON.stringify(payload);

  try {
    response = await api.post(endpoint, postObj);
  } catch (err) {
    console.log('Error caught with POST request:', err);
    return err.response.status;
  }

  return response;
};
export const postAuth = async (endpoint, payload) => {
  let token = '';
  let response = [];

  if (typeof window !== 'undefined') {
    // acquire user auth token
    token = getToken();
  }

  const headers = {
    headers: {
      Authorization: token,
    },
  };

  // TEMP UNSAFE
  const finalPayload = { AccessToken: token, ...payload };
  const postObj = qs.stringify(finalPayload);

  try {
    // response = await api.post(endpoint, postObj, headers);
    response = await api.post(endpoint, postObj);
  } catch (err) {
    console.log(
      'Error caught with POST request:',
      err,
      'with error status code of',
      err.response?.status
    );
    response = { status: err.response?.status };
    if (err.response?.status === 403 || err.response?.status === 406) {
      logoutUser(true);
    }
  }

  // console.log('Post Response:', response);
  return response;
};
