import DatePicker, { registerLocale } from 'react-datepicker';
import tw from 'date-fns/locale/zh-TW';
import { useState } from 'react';
registerLocale('tw', tw);

const Page = () => {
  const [date, setDate] = useState(null);
  const handleChange = (date) => {
    setDate(date);
  };
  return (
    <div className='form-group text-center mt-5'>
      <label htmlFor='date-d'>日期</label>
      <DatePicker
        selected={date}
        onChange={handleChange}
        dateFormat='yyyy/MM/dd'
        showYearDropdown
        showMonthDropdown
        className=''
        locale='tw'
        placeholderText='填入日期'
        id='date-d'
      />
    </div>
  );
};
export default Page;
